﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


public class MainGui : MonoBehaviour {
    static private MenuWrapper _menuWrapper;
    static private readonly Queue<System.Action> _updateOnMainThreadActions = new Queue<System.Action>();
    static private readonly Stack<MenuBase> _menus = new Stack<MenuBase>();
    static private string _log;
    static private MainGui _instance;

    static public MainGui SharedInstance {
        get {
            return _instance;
        }
    }

    static public void PushCurrentMenu(MenuBase menu) {
        _updateOnMainThreadActions.Enqueue(() => {
            _menus.Push(menu);
            _menuWrapper.SetCurrentMenu(menu);
        });
    }

    static public void PopCurrentMenu() {
        _updateOnMainThreadActions.Enqueue(() => {
            _menus.Pop();
            if (_menus.Count() == 0) {
                _menuWrapper.SetCurrentMenu(null);
            } else {
                _menuWrapper.SetCurrentMenu(_menus.Peek());
            }
        });
    }

    static public int NavigationHierarchySize() {
        return _menus.Count();
    }

    static public void AddLog(string line) {
        _updateOnMainThreadActions.Enqueue(() => {
            if (!string.IsNullOrEmpty(line)) {
                _log = DateTime.Now.ToString() + ": " + line + "\n" + _log;
            }
        });
    }

    static public string GetLog() {
        return _log;
    }

    void Awake() {
        _instance = this;
        _menuWrapper = new MenuWrapper();
    }

    void Update() {
        while (_updateOnMainThreadActions.Count > 0) {
            _updateOnMainThreadActions.Dequeue().Invoke();
        }
    }

    void OnGUI() {
        try {
            _menuWrapper.OnGuiCustom();
        } catch (Exception e) {
            Debug.Log(e.ToString());
        }
    }
}
