﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Linq;

public class Helpers {

    public static void HideGameObject(GameObject gameObject, bool isHidden) {
        Renderer[] renderers = gameObject.GetComponentsInChildren<Renderer>();
        UnityEngine.Debug.Log("renderer count: " + renderers.Length);
        foreach (Renderer r in renderers) {
            UnityEngine.Debug.Log("renderer found");
            r.enabled = !isHidden;
        }
    }

    public static void EnableGameObject(GameObject gameObject, bool enabled) {
        gameObject.SetActive(enabled);
    }

    public static string ToDelimString(Hashtable input, string delimiter) {
        return string.Join(delimiter, (from string name in input.Keys select Convert.ToString(input[name])).ToArray());
    }

    public static void SetTimeout(Action action, int timeout) {
        Thread t = new Thread(() => {
            UnityEngine.Debug.Log("Starting sleep");
            Thread.Sleep(timeout);
            UnityEngine.Debug.Log("Done sleeping");
            action.Invoke();
            UnityEngine.Debug.Log("Invoked");
        });
        t.IsBackground = true;
        t.Start();
    }
}
