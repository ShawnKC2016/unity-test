using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


public class MenuWrapper : MenuBase {
    private MenuBase _currentMenu;
    private MenuBase _mainMenu;
    private HeaderMenu _headerMenu;
    private FooterMenu _footerMenu;
    private Texture2D _navBackgroundTexture;
    private Vector2 _scrollPosition = Vector2.zero;

    public string status;
    public string statusError;

    public MenuWrapper() {
        _headerMenu = new HeaderMenu();
        _headerMenu.Load();
        _footerMenu = new FooterMenu();
        _footerMenu.Load();
        _mainMenu = new MainMenu();
        SetCurrentMenu(_mainMenu);
        _navBackgroundTexture = MakeTex(1, 1, new Color(1.0f, 1.0f, 1.0f, 0.20f));
    }

    public void SetCurrentMenu(MenuBase menu) {
        if (_currentMenu != null) {
            _currentMenu.Unload();
            _currentMenu = null;
        }

        if (menu == null) {
            _currentMenu = _mainMenu;
        } else {
            _currentMenu = menu;
        }

        if (_currentMenu != null) {
            _currentMenu.Load();
        }
    }

    private void Header() {
        _headerMenu.OnGuiCustom();
    }

    private void NavigationBar() {
        float buttonWidth = buttonHeight * 0.5f;

        GUIStyle tempTextStyle = new GUIStyle(labelTextStyle);
        tempTextStyle.alignment = TextAnchor.MiddleCenter;
        tempTextStyle.fontSize = 24;
        // tempTextStyle.fontStyle = FontStyle.Bold;

        GUIStyle navStyle = new GUIStyle();
        navStyle.normal.background = _navBackgroundTexture;
        navStyle.fixedHeight = buttonWidth * 1.5f;

        GUILayout.Space(10);

        GUILayout.BeginHorizontal(navStyle);

        if (MainGui.NavigationHierarchySize() > 0) {
            GUIStyle tempButtonStyle = new GUIStyle(GUI.skin.button);
            tempButtonStyle.fixedWidth = buttonWidth;
            tempButtonStyle.fixedHeight = buttonWidth;

            Color prev = GUI.backgroundColor;
            GUI.backgroundColor = Color.red;
            GUI.backgroundColor = prev;

            if (GUILayout.Button("<=", tempButtonStyle)) {
                MainGui.PopCurrentMenu();
            }

            tempTextStyle.contentOffset = new Vector2(-buttonWidth / 2.0f - tempButtonStyle.padding.horizontal, 0);
        }

        GUILayout.Label(_currentMenu.Title(), tempTextStyle, GUILayout.ExpandWidth(true));

        GUILayout.EndHorizontal();

        GUILayout.Space(10);
    }

    private void Content() {
        GUILayout.BeginVertical();

        _currentMenu.OnGuiCustom();

        GUILayout.EndVertical();
    }

    private void Footer() {
        _footerMenu.OnGuiCustom();
    }

    public override void OnGuiCustom() {
        // set up scaling
        GUI.matrix = Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(ScaleFactor, ScaleFactor, 1));

        GUILayout.BeginArea(new Rect(0, 0, WindowWidth, WindowHeight));

        Header();
        NavigationBar();

#if UNITY_IOS || UNITY_ANDROID || UNITY_WP8
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved) {
            _scrollPosition.y += (Input.GetTouch(0).deltaPosition.y / ScaleFactor);
        }
#endif
        _scrollPosition = GUILayout.BeginScrollView(_scrollPosition, GUILayout.MinWidth(mainWindowFullWidth), GUILayout.ExpandHeight(true));

        Content();

        GUILayout.EndScrollView();

        GUILayout.FlexibleSpace();

        Footer();

        GUILayout.EndArea();
    }
}
