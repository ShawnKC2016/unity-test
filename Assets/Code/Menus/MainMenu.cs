using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


public class MainMenu : MenuBase {
    private readonly Queue<System.Action> _updateOnMainThreadActions = new Queue<System.Action>();

    private string[] AppZoneListing = {"Test", "Live", "Rewarded", "Test v3", "Rewarded v3"};

    // Android, iOS
    private string[,] AppIds = {{"appc0e447bbadfc446890", "app92d05942abec4c6b8c"},
                                {"appefc287c79650411589", "app92d05942abec4c6b8c"},
                                {"app620f6738672041e5b7", "app92d05942abec4c6b8c"},
                                {"app1a297972a9114c7285", "app92d05942abec4c6b8c"},
                                {"app1a297972a9114c7285", "app92d05942abec4c6b8c"}};

    // Android QA test zones
    private string[,] Zones = {{"vza9f78c5bbbb54a7ca5", "vz54bde6e9d6b94b4d90"},
                                {"vzf154b16e12024024a8", "vz7ee0072f390e4b6785"},
                                {"vze3e104bb0caa47579d", "vzc55a21f3da6a4c43ab"},
                                {"vz8c5551c0515d4585ba", "vz737c92a61db24ece8a"},
                                {"vz8c5551c0515d4585ba", "vzcee1a83e1f14412ebc"}};

    public override string Title() {
        return "Main Menu";
    }

    public override void Load() {
        Debug.Log("MainMenu Loaded!");
    }

    public override void Unload() {
        Debug.Log("MainMenu Unloaded!");
    }

    public override void OnGuiCustom() {
        base.OnGuiCustom();

        int osIndex = -1;

        #if UNITY_IOS
        osIndex = 1;
        #elif UNITY_ANDROID
        osIndex = 0;
        #endif

        if (osIndex < 0) {
            return;
        }

        while (_updateOnMainThreadActions.Count > 0) {
            _updateOnMainThreadActions.Dequeue().Invoke();
        }

        string buttonLabel = "";

        GUILayout.Label(string.Format("AdColony App Id: {0}", ApplicationManager.SharedInstance.AppId), labelTextStyle, GUILayout.ExpandWidth(false));
        GUILayout.Label(string.Format("AdColony Zone Id: {0}", ApplicationManager.SharedInstance.ZoneId), labelTextStyle, GUILayout.ExpandWidth(false));

        // Top Section
        GUILayout.BeginHorizontal();
        GUILayout.Label("Status:", labelTextStyle, GUILayout.ExpandWidth(false));
		GUIStyle tempTextStyle = new GUIStyle(labelTextStyle);
        string statusText = "None";
        if (!string.IsNullOrEmpty(ApplicationManager.SharedInstance.Status)) {
            tempTextStyle.normal.textColor = Color.yellow;
            statusText = ApplicationManager.SharedInstance.Status;
        }
        GUILayout.Label(statusText, tempTextStyle, GUILayout.ExpandWidth(false));
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Reward:", labelTextStyle, GUILayout.ExpandWidth(false));
        if (ApplicationManager.SharedInstance.RewardQuantity > 0) {
            GUILayout.Label(string.Format("{0} {1}", ApplicationManager.SharedInstance.RewardQuantity, ApplicationManager.SharedInstance.RewardName), labelTextStyle, GUILayout.ExpandWidth(false));
        } else {
            GUILayout.Label("-", labelTextStyle, GUILayout.ExpandWidth(false));
        }
        GUILayout.EndHorizontal();

        GUILayout.Space(10);

        // Initialize
        GUILayout.Label("Configure:", labelTextStyle, GUILayout.ExpandWidth(false));
        GUILayout.BeginHorizontal();
        for (int i = 0; i < AppZoneListing.Length; i++) {
            string appId = AppIds[i, osIndex];
            string zoneId = Zones[i, osIndex];
            if (Button(AppZoneListing[i])) {
                ApplicationManager.SharedInstance.Status = "Configuring...";
                ApplicationManager.SharedInstance.InitializeAdColony(appId, zoneId);
            }            
        }
        GUILayout.EndHorizontal();

        // Interstitial Section
        GUILayout.Space(5);
        GUILayout.Label("Ad:", labelTextStyle, GUILayout.ExpandWidth(false));
        
        GUILayout.BeginHorizontal();
        
        GUI.enabled = ApplicationManager.SharedInstance.IsConfigured;
        buttonLabel = string.Format("Request");
        if (Button(buttonLabel)) {
            ApplicationManager.SharedInstance.Status = "Requesting ad...";
            Debug.Log("Request Ad");
            AdColony.Ads.RequestInterstitialAd(ApplicationManager.SharedInstance.ZoneId, null);
        }
        GUILayout.Space(10);

        GUI.enabled = ApplicationManager.SharedInstance.IsAvailable;
        buttonLabel = string.Format("Show");
        if (Button(buttonLabel)) {
            ApplicationManager.SharedInstance.Status = "Showing ad...";
            Debug.Log("Showing Ad: ad: " + ((ApplicationManager.SharedInstance.TheAd == null) ? "null" : "valid"));
            AdColony.Ads.ShowAd(ApplicationManager.SharedInstance.TheAd);
        }
        GUILayout.Space(10);

        GUI.enabled = ApplicationManager.SharedInstance.IsAvailable;
        buttonLabel = string.Format("Show & Cancel");
        if (Button(buttonLabel)) {
            ApplicationManager.SharedInstance.Status = "Showing ad...";
            Debug.Log("Showing Ad: ad: " + ((ApplicationManager.SharedInstance.TheAd == null) ? "null" : "valid"));
            AdColony.Ads.ShowAd(ApplicationManager.SharedInstance.TheAd);
            Helpers.SetTimeout(() => {
                ApplicationManager.SharedInstance.Status = "Cancelling ad...";
                Debug.Log("Cancelling Ad");
                ApplicationManager.SharedInstance.EnqueueAction(() => { AdColony.Ads.CancelAd(ApplicationManager.SharedInstance.TheAd); });
            }, 3000);
        }

        GUILayout.EndHorizontal();

        // Miscellaneous Section

        GUILayout.Space(15);

        GUI.enabled = true;
        if (Button("Log")) {
            MainGui.PushCurrentMenu(new LogMenu());
        }
    }
}
