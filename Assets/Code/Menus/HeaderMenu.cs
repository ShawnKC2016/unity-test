using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


public class HeaderMenu : MenuBase {
    public string status;
    public string statusError;

    private string _currentApplicationVersion;
    private string _sdkVersion;
    private readonly Queue<System.Action> _updateOnMainThreadActions = new Queue<System.Action>();

    public override void Load() {
    }

    public override void Unload() {
    }

    public override void OnGuiCustom() {
#if UNITY_EDITOR
		_currentApplicationVersion = UnityEditor.PlayerSettings.bundleVersion;
#else
		_currentApplicationVersion = RuntimeWrapper.RuntimeBundleVersion;
#endif

        if (ApplicationManager.SharedInstance.IsConfigured) {
            _sdkVersion = AdColony.Ads.GetSDKVersion();
        }

		while (_updateOnMainThreadActions.Count > 0) {
			_updateOnMainThreadActions.Dequeue().Invoke();
		}

        GUI.enabled = true;
        GUILayout.Space(10);

        GUILayout.Label(string.Format("AdColony Unity Test Shell v{0}", _currentApplicationVersion), labelTextStyle, GUILayout.ExpandWidth(false));
        GUILayout.Label(string.Format("AdColony SDK v{0}", _sdkVersion), labelTextStyle, GUILayout.ExpandWidth(false));
    }
}
