using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public interface IMenuBase {
    void OnGuiCustom();
    string Title();
}

public class MenuBase : IMenuBase {
    protected GUIStyle textStyle = new GUIStyle();
    protected GUIStyle labelTextStyle = new GUIStyle();

// #if UNITY_IOS || UNITY_ANDROID || UNITY_WP8
    public int buttonHeight = 60;
    public int mainWindowWidth {
        get {
            return WindowWidth - 30;
        }
    }
    public int mainWindowFullWidth {
        get {
            return WindowWidth;
        }
    }

// #else
//     public int buttonHeight = 24;
//     public static int mainWindowWidth = 500;
//     public static int mainWindowFullWidth = 530;
// #endif

    protected MenuBase() {
        textStyle.alignment = TextAnchor.UpperLeft;
        textStyle.wordWrap = true;
        textStyle.padding = new RectOffset(0, 0, 0, 0);
        textStyle.stretchHeight = true;
        textStyle.stretchWidth = false;

        labelTextStyle.alignment = TextAnchor.MiddleLeft;
        labelTextStyle.wordWrap = true;
        labelTextStyle.padding = new RectOffset(0, 0, 0, 0);
        labelTextStyle.stretchHeight = true;
        labelTextStyle.stretchWidth = false;
        labelTextStyle.normal.textColor = Color.white;
    }

    protected bool Button(string label) {
        // GUIStyle style = GUI.skin.button;
        // style.alignment = TextAnchor.MiddleCenter;
        return GUILayout.Button(
            label,
            // style,
            GUILayout.MinHeight(buttonHeight),
            GUILayout.MaxWidth(mainWindowFullWidth)
            );
    }

    protected void LabelAndTextField(string label, ref string text) {
        GUILayout.BeginHorizontal();
        GUILayout.Label(label, labelTextStyle, GUILayout.MaxWidth(150));
        text = GUILayout.TextField(text);
        GUILayout.EndHorizontal();
    }

    protected bool IsHorizontalLayout() {
#if UNITY_IOS || UNITY_ANDROID || UNITY_WP8
        return Screen.orientation == ScreenOrientation.Landscape;
#else
        return true;
#endif
    }

    protected float ScaleFactor {
        get {
            return Screen.dpi / 150.0f; // 180 ok
        }
    }
    protected int WindowHeight {
        get {
            return (int)((float)Screen.height / ScaleFactor);
            // return (int)(IsHorizontalLayout() ? (float)Screen.width / ScaleFactor : (float)Screen.height / ScaleFactor);
        }
    }

    protected int WindowWidth {
        get {
            return (int)((float)Screen.width / ScaleFactor);
            // return (int)(IsHorizontalLayout() ? (float)Screen.height / ScaleFactor : (float)Screen.width / ScaleFactor);
        }
    }

    protected Texture2D MakeTex(int width, int height, Color col) {
        Color[] pix = new Color[width * height];

        for(int i = 0; i < pix.Length; i++) {
            pix[i] = col;
        }

        Texture2D result = new Texture2D(width, height, TextureFormat.RGBA32, false);
        result.SetPixels(pix);
        result.Apply();

        return result;
    }

    protected Texture2D MakeTex(int width, int height, Color textureColor, RectOffset border, Color bordercolor) {
        int widthInner = width;
        width += border.left;
        width += border.right;

        Color[] pix = new Color[width * (height + border.top + border.bottom)];

        for (int i = 0; i < pix.Length; i++) {
            if (i < (border.bottom * width)) {
                pix[i] = bordercolor;
            } else if (i >= ((border.bottom * width) + (height * width))) {
                pix[i] = bordercolor; // Border Top
            } else { //Center of Texture
                if ((i % width) < border.left) {
                    pix[i] = bordercolor; // Border left
                } else if ((i % width) >= (border.left + widthInner)) {
                    pix[i] = bordercolor; // Border right
                } else {
                    pix[i] = textureColor; //Color texture
                }
            }
        }

        Texture2D result = new Texture2D(width, height + border.top + border.bottom, TextureFormat.RGBA32, false);
        result.SetPixels(pix);
        result.Apply();

        return result;
    }

    public virtual void OnGuiCustom() {

    }

    public virtual string Title() {
        return null;
    }

    public virtual void Load() {
        // Register all event handlers and such
    }

    public virtual void Unload() {
        // Unregister all event handlers and such
    }
}
