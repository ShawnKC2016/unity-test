using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


public class LogMenu : MenuBase {
    public override string Title() {
        return "Log History";
    }

    public override void OnGuiCustom() {
        GUI.enabled = true;
        GUIStyle topLeft = new GUIStyle(labelTextStyle);
        topLeft.alignment = TextAnchor.UpperLeft;
        GUILayout.Label(MainGui.GetLog(), topLeft);
    }
}
