using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.InteropServices;

public class ApplicationManager : MonoBehaviour {
    // -------------------------------------------------------------------------
    // Public
    // -------------------------------------------------------------------------
    public string Version = "100";

    public string AppId = "";
    public string ZoneId = "";

    public string RewardName;

    public int RewardQuantity;

    public bool IsConfigured = false;
    public bool IsAvailable = false;
    public AdColony.InterstitialAd TheAd = null;

    // -------------------------------------------------------------------------
    // Private
    // -------------------------------------------------------------------------

    private readonly Queue<System.Action> _updateOnMainThreadActions = new Queue<System.Action>();
    private System.Object _updateOnMainThreadActionsLock = new System.Object();

    private bool _initialized = false;
    public bool IsInitialized { get { return _initialized; } }

    private string _status = "";
    public string Status { get { return _status; } set { _status = value; } }

    private static ApplicationManager _sharedInstance;

    // -------------------------------------------------------------------------
    // Singleton
    // -------------------------------------------------------------------------

    public static ApplicationManager SharedInstance {
        get {
            if (!_sharedInstance) {
                _sharedInstance = (ApplicationManager)FindObjectOfType(typeof(ApplicationManager));
            }

            if (!_sharedInstance) {
                GameObject singleton = new GameObject();
                _sharedInstance = singleton.AddComponent<ApplicationManager>();
                singleton.name = "ApplicationManager";
                DontDestroyOnLoad(singleton);
            }

            return _sharedInstance;
        }
    }

    private ApplicationManager() {
    }

    // -------------------------------------------------------------------------
    // Unity
    // -------------------------------------------------------------------------

    void Awake() {
        Debug.Log("[ApplicationManager] Awake");

        IncrementBuildNumber();

        if (gameObject == SharedInstance.gameObject) {
            DontDestroyOnLoad(gameObject);
        } else {
            Destroy(gameObject);
        }
    }

    void Start() {
        Debug.Log("[ApplicationManager] Start");

        AdColony.Ads.OnConfigurationCompleted += (List<AdColony.Zone> zones_) => {
            string msg = "AdColony.Ads.OnConfigurationCompleted called";
            EnqueueAction(() => {
                Status = "Configured";
                Debug.Log(msg);
                MainGui.AddLog(msg);
                IsConfigured = true;
            });
        };

        AdColony.Ads.OnRequestInterstitial += (AdColony.InterstitialAd ad) => {
            string msg = "AdColony.Ads.OnRequestInterstitial called";
            EnqueueAction(() => {
                TheAd = ad;
                IsAvailable = true;
                Status = "Request filled in zone: " + ad.ZoneId;
                Debug.Log(msg);
                MainGui.AddLog(msg);
            });
        };

        AdColony.Ads.OnRequestInterstitialFailedWithZone += (string zoneId) => {
            string msg = "AdColony.Ads.OnRequestInterstitialFailed called";
            EnqueueAction(() => {
                IsAvailable = false;
                Status = "Failed request in zone: " + zoneId;
                Debug.Log(msg);
                MainGui.AddLog(msg);
            });
        };

        AdColony.Ads.OnOpened += (AdColony.InterstitialAd ad) => {
            string msg = "AdColony.Ads.OnOpened called";
            EnqueueAction(() => {
                IsAvailable = false;
                Status = "Opened ad";
                Debug.Log(msg);
                MainGui.AddLog(msg);
            });
        };

        AdColony.Ads.OnClosed += (AdColony.InterstitialAd ad) => {
            string msg = "AdColony.Ads.OnClosed called, expired: " + ad.Expired;
            EnqueueAction(() => {
                IsAvailable = false;
                Status = "Closed ad";
                Debug.Log(msg);
                MainGui.AddLog(msg);
                TheAd = null;
            });
        };

        AdColony.Ads.OnExpiring += (AdColony.InterstitialAd ad) => {
            string msg = "AdColony.Ads.OnExpiring called";
            EnqueueAction(() => {
                IsAvailable = false;
                Status = "Expired ad";
                Debug.Log(msg);
                MainGui.AddLog(msg);
                TheAd = null;
            });
        };

        AdColony.Ads.OnIAPOpportunity += (AdColony.InterstitialAd ad_, string iapProductId_, AdColony.AdsIAPEngagementType engagement_) => {
            string msg = "AdColony.Ads.OnIAPOpportunity called";
            EnqueueAction(() => {
                Status = "IAP Opportunity called";
                Debug.Log(msg);
                MainGui.AddLog(msg);
            });
        };

        AdColony.Ads.OnRewardGranted += (string zoneId, bool success, string name, int amount) => {
            string msg = string.Format("AdColony.Ads.OnRewardGranted called\n\tZoneId: {0}\n\tsuccess: {1}\n\tname: {2}\n\tamount: {3}", zoneId, success, name, amount);
            EnqueueAction(() => {
                Debug.Log(msg);
                MainGui.AddLog(msg);
                if (success) {
                    Status = "Reward granted, " + amount + " " + name;
                    RewardName = name;
                    RewardQuantity += amount;
                } else {
                    Status = "Reward failed";
                }
            });
        };

        AdColony.Ads.OnCustomMessageReceived += (string type, string message) => {
            string msg = string.Format("AdColony.Ads.OnCustomMessageReceived called\n\ttype: {0}\n\tmessage: {1}", type, message);
            EnqueueAction(() => {
                Status = "Custom message received: " + type + ", " + message;
                Debug.Log(msg);
                MainGui.AddLog(msg);
            });
        };
    }

    void Update() {
        if (_updateOnMainThreadActions.Count > 0) {
            System.Action action;
            do {
                action = null;
                lock (_updateOnMainThreadActionsLock) {
                    if (_updateOnMainThreadActions.Count > 0) {
                        action = _updateOnMainThreadActions.Dequeue();
                    }
                }
                if (action != null) {
                    action.Invoke();
                }
            } while (action != null);
        }
    }

    public void EnqueueAction(System.Action action) {
        lock (_updateOnMainThreadActionsLock) {
            _updateOnMainThreadActions.Enqueue(action);
        }
    }

    private void IncrementBuildNumber() {
    }

    public void InitializeAdColony(string appId, string zoneId) {
        Debug.Log("InitializeAdColony " + appId + "/" + zoneId);
        AppId = appId;
        ZoneId = zoneId;

        // Set some test app options with metadata.
        AdColony.AppOptions appOptions = new AdColony.AppOptions();
        appOptions.UserId = "foo";
        appOptions.AdOrientation = AdColony.AdOrientationType.AdColonyOrientationAll;
        appOptions.SetOption("test_key", "Happy Fun Time!");
        #if UNITY_IOS
        appOptions.SetOption("reconfigurable", 1);
        #elif UNITY_ANDROID
        appOptions.SetOption("reconfigurable", true);
        #endif

        AdColony.UserMetadata metadata = new AdColony.UserMetadata();
        metadata.Age = 35;
        metadata.Gender = "Male";
        metadata.Interests = new List<string> {"gaming", "hiking"};
        metadata.Latitude = 47.6469425;
        metadata.Longitude = -122.2004281;
        metadata.ZipCode = "98033";
        metadata.HouseholdIncome = 75000;
        metadata.MaritalStatus = "Single";
        metadata.EducationLevel = "Bachelors";
        metadata.SetMetadata("test_key_02", "Happy Meta Time?");
        appOptions.Metadata = metadata;

        AdColony.Ads.Configure(appId, appOptions, new string[]{zoneId});
    }
}
