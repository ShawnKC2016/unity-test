using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;
using System.IO;

[InitializeOnLoad]
public class ApplicationVersionBuilder
{
    [PostProcessBuildAttribute(1)]
    public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject) {
        Debug.Log("Application Build Postprocessor");
        IncreaseBuild();
    }

    static void IncrementVersion(int majorIncr, int minorIncr, int hotfixIncr, int buildIncr) {
        string versionText = PlayerSettings.bundleVersion;
        versionText = versionText.Trim(); //clean up whitespace if necessary
        string[] lines = versionText.Split('.');

        int majorVersion = 0;
        int minorVersion = 0;
        int hotfixVersion = 0;
        int buildVersion = 0;

        if (lines.Length > 0) majorVersion = int.Parse(lines[0]);
        if (lines.Length > 1) minorVersion = int.Parse(lines[1]);
        if (lines.Length > 2) hotfixVersion = int.Parse(lines[2]);
        if (lines.Length > 3) buildVersion = int.Parse(lines[3]);

        majorVersion += majorIncr;
        minorVersion += minorIncr;
        hotfixVersion += hotfixIncr;
        buildVersion += buildIncr;

        versionText = string.Format("{0}.{1}.{2}.{3}", majorVersion, minorVersion, hotfixVersion, buildVersion);
        PlayerSettings.bundleVersion = versionText;

		int versionCode = majorVersion * 10000000 + minorVersion * 100000 + hotfixVersion * 1000 + buildVersion;
		PlayerSettings.Android.bundleVersionCode = 14;//versionCode;
		PlayerSettings.iOS.buildNumber = versionCode.ToString();

        string fullPath = Application.dataPath + "/Code/RuntimeWrapper.cs";
        CreateNewBuildVersionClassFile(fullPath, GenerateCode());
    }

    [MenuItem("Tools/Compass Test Shell/Increase Major Version")]
    private static void IncreaseMajor() {
        IncrementVersion(1, 0, 0, 0);
    }

    [MenuItem("Tools/Compass Test Shell/Increase Minor Version")]
    private static void IncreaseMinor() {
        IncrementVersion(0, 1, 0, 0);
    }

    [MenuItem("Tools/Compass Test Shell/Increase Hotfix Version")]
    private static void IncreaseHotfix() {
        IncrementVersion(0, 0, 1, 0);
    }

    [MenuItem("Tools/Compass Test Shell/Increase Build Version")]
    private static void IncreaseBuild() {
        IncrementVersion(0, 0, 0, 1);
    }

    static string GenerateCode() {
        string code = "// GENERATED CODE\n";
        code += "public class RuntimeWrapper {\n";
        code += "    public static string RuntimeBundleVersion = \"" + PlayerSettings.bundleVersion + "\";\n";
        code += "}\n";
        return code;
    }

    static void CreateNewBuildVersionClassFile(string fullPath, string code) {
        if (System.String.IsNullOrEmpty(code)) {
            Debug.Log ("Code generation stopped, no code to write.");
        }
        System.IO.File.WriteAllText(fullPath, code);
    }
}
